def arctan(x, N):
	numerator = (-1) ** N
	denominator = (2 * N) + 1
	multiplyer = x ** ((2 * N) + 1)
	return (numerator / denominator) * multiplyer 

def arctanLoop(x, N):
	if N == 0:
		return x
	else:
		return arctan(x, N) + arctanLoop(x, (N - 1))

def question():
	print ("\n\
a) Produce a function that approximates arctan(x) for N degrees of accuracy \n\
using the given formula for |x| <= 1 ")

	inputX = 3
	while (abs(inputX) > 1):
		inputX = float(input("Enter x vlaue for arctan(x) where |x| <= 1 "))
	inputAccuracy = int(input("Enter degree of accuracy "))
	print (arctanLoop(inputX, inputAccuracy))
