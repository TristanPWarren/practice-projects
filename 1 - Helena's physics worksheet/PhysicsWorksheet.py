#!/usr/bin/env python3

import a
import b
import c


print ("Questions for some Physics sheet")

validQuestion = ["a", "b", "c", "A", "B", "C"]

def getQuestion():
	inputQuestion = str(input("Pick a question number (a, b or c) "))
	if inputQuestion not in validQuestion:
		print ("Invalid question number")
		getQuestion()
	else: 
		if (inputQuestion == "a") or (inputQuestion == "A"):
			a.question()
		elif (inputQuestion == "b") or (inputQuestion == "B"):
			b.question()
		elif (inputQuestion == "c") or (inputQuestion == "C"):
			c.question()

while True:
	getQuestion()