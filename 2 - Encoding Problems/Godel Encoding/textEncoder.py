def getSpecificLine(n):
	primes = open("sortedPrimes.txt", "r")
	counter = 0
	for line in primes.readlines():
		if counter == n:
			primes.close()
			return int(line)
		else:
			counter += 1

def checkValidInput(inputText):
	for letter in inputText:
		if ((ord(letter) < 48) or (ord(letter) > 122)) and (ord(letter) != 32):
			return True
	return False


def encoder():
	print ("Text can include 0-9, a-z, A-Z and \" \"")
	inputText = "#"
	while checkValidInput(inputText):
		inputText = str(input("Enter string to be encoded "))

	chrachterPosition = 0
	total = 1
	for chrachter in inputText:
		t = (int(getSpecificLine(chrachterPosition)))
		if chrachter == " ":
			letterOrd = 47
		else:
			letterOrd = ord(chrachter)

		total *= (t ** (letterOrd - 46))
		chrachterPosition += 1

	print (total)
