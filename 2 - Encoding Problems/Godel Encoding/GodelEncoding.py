#!/usr/bin/env python3
import textEncoder
import textDecoder

validInputs = ["E", "e", "Encoder", "encoder", "D", "d", "Decoder", "decoder"]

while True:
	goodMenu = True
	while goodMenu:
		menuSelector = str(input("'E'ncoder or 'D'ecoder? "))
		if menuSelector in validInputs:
			goodMenu = False

	if (menuSelector == "E") or (menuSelector == "e") or (menuSelector == "Encoder") or (menuSelector == "encoder"):
		textEncoder.encoder()
	else:
		textDecoder.decoder()


# 48 122
# Make check that ensures people enter 0 - 9 - A - z only and " "
# Check for valid decode input
# Improve performace in decoder by not dividing the whole number each time
# Encode larger messages
# Decoder only accepts numbers
