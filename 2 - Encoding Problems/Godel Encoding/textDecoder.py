import textEncoder

def findLengthOfWord(n):
	largestPrime = 2
	primeIndicator = 0

	while (n % largestPrime == 0):
		largestPrime = textEncoder.getSpecificLine(primeIndicator)
		primeIndicator += 1
	return (primeIndicator - 1)

def getAsciiNumber(primePosition, number):
	prime = textEncoder.getSpecificLine(primePosition)
	asciiNumber = 0
	while (number % prime == 0):
		asciiNumber += 1
		number = number // prime
		print (number)
	return (number, asciiNumber)

def checkValidInput(inputText):
	for letter in inputText:
		if (ord(letter) < 48) or (ord(letter) > 57):
			return True
	return False

def decoder():
	invalidInput = True
	while invalidInput:
		inputText = str(input("Enter string to be decoded "))
		invalidInput = checkValidInput(inputText)
		if inputText == '':
			invalidInput = True

	inputText = int(inputText)
	finalString = ""

	for i in range(0, findLengthOfWord(inputText)):
		(inputText, asciiNumber) = (getAsciiNumber(i, inputText))
		if (asciiNumber + 46) == 47:
			finalString += " "
		else:
			finalString += (chr(asciiNumber + 46))
		# print ("      " + str(inputText))

	print ("      " + finalString)
