#!/usr/bin/env python3
import textEncoding
import textDecoding

# List of valid inputs to select the function
validInputs = ["E", "e", "Encoder", "encoder", "D", "d", "Decoder", "decoder"]

# Infinate loop for selecting encoder or decoder functions
while True:
	goodMenu = True
	while goodMenu:
		menuSelector = str(input("'E'ncoder or 'D'ecoder? "))
		if menuSelector in validInputs:
			goodMenu = False

	if (menuSelector == "E") or (menuSelector == "e") or (menuSelector == "Encoder") or (menuSelector == "encoder"):
		textEncoding.encoder()
	else:
		textDecoding.decoder()