# Checks that the inout is valid
def checkValidInput(inputText):
	for letter in inputText:
		if ((ord(letter) < 0) or (ord(letter) > 127)):
			return True
	return False


# Gets the n'th number in the list of base64 characters
def getConvertedLetter(number):
	base64 = open("base64Charachters.txt", "r")
	line = base64.readline()
	return line[number]


# Makes each character into an 8 character long binary string 
# (may add extra 0's) and then concatonates them together
def makeStringIntoBinary(string):
	binaryString = ""
	for letter in string:
		singleLetterInBinary = format(ord(letter), 'b')
		while len(singleLetterInBinary) != 8:
			singleLetterInBinary = "0" + singleLetterInBinary
		binaryString += singleLetterInBinary

	return binaryString


# Splits binary number into 4 and then converts that into a base64 character
def makeBinaryToString(binary):
	returnString = ""
	for i in range(1, 5):
		returnString += getConvertedLetter(int((binary[((i-1)*6):(i*6)]), 2))
		
	return returnString


# Calls functions based on if the length of the string to convert it into base64 
# characters and adds ='s if nedded
def convertTo64(smallText):
	returnText = ""
	if len(smallText) == 3:
		print (makeStringIntoBinary(smallText))
		returnText = makeBinaryToString(makeStringIntoBinary(smallText))
	elif len(smallText) == 2:
		returnText = makeBinaryToString(makeStringIntoBinary(smallText) + "00000000")[0:3] + "="
	elif len(smallText) == 1:
		returnText = makeBinaryToString(makeStringIntoBinary(smallText) + "0000000000000000")[0:2] + "=="

	return returnText

# Entry point for decoder
# Gets inpout text and makes sure that it's valid
# Calls functions to convert text
def encoder():
	print ("Text can include 0-9, a-z, A-Z and \" \"")
	inputText = (chr(128))
	while checkValidInput(inputText):
		inputText = str(input("Enter string to be encoded "))

	# Splits text into groups of 3
	counter = 0
	smallString = ""
	finalString = ""
	for letter in inputText:
		if counter < 2:
			smallString += letter
			counter += 1
		else:
			smallString += letter
			finalString += convertTo64(smallString)
			smallString = ""
			counter = 0

	finalString += convertTo64(smallString)

	print (finalString)
