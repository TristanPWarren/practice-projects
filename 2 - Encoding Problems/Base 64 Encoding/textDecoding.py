# Validates text input contains only characters in the base64 encoding list
def checkValidInput(inputText):
	base64 = open("base64Charachters.txt", "r")
	line = base64.readline()
	for letter in inputText:
		letterBase64 = True
		for base64letter in line:
			if letter == base64letter:
				letterBase64 = False

		if letterBase64 == True:
			return True

	return False


# Gets the number that represents the base64 character
def getBase64Number(character):
	base64 = open("base64Charachters.txt", "r")
	line = base64.readline()
	counter = 0 
	for character64 in line:
		if (character64 == character):
			return counter
		else:
			counter += 1



# Takes a string 4 charcters long and makes a binary number either
# 12, 18 or 24 characters long (pads with 0's if needed)
def makeStringIntoBinary(string):
	binaryString = ""
	for letter in string:
		if ord(letter) != 61:
			singleLetterInBinary = format(getBase64Number(letter), 'b')
			while len(singleLetterInBinary) != 6:
				singleLetterInBinary = "0" + singleLetterInBinary
			binaryString += singleLetterInBinary

	print (binaryString)
	return binaryString


# Takes a binary number that is 12, 18 or 24 characters long
# and splits it into chunks 8 big and then converts to decimal
# and then to a character
def makeBinaryToString(binary):
	returnString = ""
	for i in range(1, int((len(binary) / 6))):
		returnString += chr(int((binary[((i-1)*8):(i*8)]), 2))
			
	return returnString


# Converts text to ASCII text
def convertToASCII(smallText):
	returnText = ""
	returnText = makeBinaryToString(makeStringIntoBinary(smallText))

	return returnText


# Entry point for decoder
# Gets inpout text and makes sure that it's valid
# Calls functions to convert text
def decoder():
	print ("Text must be in base64 encoding format")
	inputText = (chr(128))
	while checkValidInput(inputText):
		inputText = str(input("Enter string to be decoded "))

	# Splits text into groups of four characters
	counter = 0
	smallString = ""
	finalString = ""
	for letter in inputText:
		if counter < 3:
			smallString += letter
			counter += 1
		else:
			smallString += letter
			finalString += convertToASCII(smallString)
			smallString = ""
			counter = 0

	finalString += convertToASCII(smallString)

	print (finalString)
