#!/usr/bin/env python3
import orbit
import orbit2
import orbit3
import orbit4

def getSatSpeed():
	returnValue = float(input("Satelite Speed (eg. 4) ")) 
	return returnValue

def getOrbitHeight():
	returnValue = float(input("Orbit Height (eg. 200) ")) 
	return returnValue

def getPlanetMass():
	returnValue = float(input("Planet Mass (eg. 40) ")) 
	return returnValue

def getPlanetRadius():
	returnValue = float(input("Planet Radius (eg. 15) ")) 
	return returnValue

def getDirection():
	returnValue = float(input("Direction (eg. 80) ")) 
	return returnValue

def getBoost():
	returnValue = float(input("Boost (eg. 0.001) ")) 
	return returnValue

def getLengthOfBoost():
	returnValue = float(input("Length Of Boost (eg. 1000) ")) 
	return returnValue

def getNewPlanetX():
	returnValue = float(input("New Planet X Location (eg. 300) ")) 
	return returnValue

def getNewPlanetY():
	returnValue = float(input("New Planet Y Location (eg. 300) ")) 
	return returnValue

# Runs orbit
def choose1():
	validVersion = ["B", "b", "0", "1"]

	goodMenu = True
	while goodMenu == True:
		versionSelect = str(input("Select simulation: \n\
	B - Back \n\
	0 - Custom \n\
	1 - Circular Orbit "))

		if versionSelect in validVersion:
			goodMenu = False

	if (versionSelect == "B") or (versionSelect == "b"):
		print (" ")
	elif int(versionSelect) == 0:
		a = getSatSpeed()
		b = getOrbitHeight()
		c = getPlanetMass()
		d = getPlanetRadius()
		orbit.orbit(a, b, c, d)
	elif int(versionSelect) == 1:
		orbit.orbit(7,200,93.3069655,15)

# Runs orbit2
def choose2():
	validVersion = ["B", "b", "0", "1", "2"]

	goodMenu = True
	while goodMenu == True:
		versionSelect = str(input("Select simulation: \n\
	B - Back \n\
	0 - Custom \n\
	1 - Slow forming ring "))

		if versionSelect in validVersion:
			goodMenu = False

	if (versionSelect == "B") or (versionSelect == "b"):
		print (" ")
	elif int(versionSelect) == 0:
		a = getPlanetMass()
		b = getPlanetRadius()
		c = getOrbitHeight()
		d = getSatSpeed()
		e = getDirection()
		orbit2.orbit(a, b, c, d, e)
	elif int(versionSelect) == 1:
		orbit2.orbit(60,15,200,5,60)

# Runs orbit3
def choose3():
	validVersion = ["B", "b", "0", "1", "2"]

	goodMenu = True
	while goodMenu == True:
		versionSelect = str(input("Select simulation: \n\
	B - Back \n\
	0 - Custom \n\
	1 - Tight Spiral \n\
	2 - Who knows what this is called "))

		if versionSelect in validVersion:
			goodMenu = False

	if (versionSelect == "B") or (versionSelect == "b"):
		print (" ")
	elif int(versionSelect) == 0:
		a = getPlanetMass()
		b = getPlanetRadius()
		c = getOrbitHeight()
		d = getSatSpeed()
		e = getDirection()
		f = getBoost()
		g = getLengthOfBoost()
		orbit3.orbit(a, b, c, d, e, f, g)
	elif int(versionSelect) == 1:
		orbit3.orbit(12.3,15,30,7,100,0.001,2800)
	elif int(versionSelect) == 2:
		orbit3.orbit(60,15,200,4,90,0.001,100)

# Runs Orbit4
def choose4():
	validVersion = ["B", "b", "0", "1", "2", "3 "]

	goodMenu = True
	while goodMenu == True:
		versionSelect = str(input("Select simulation: \n\
	B - Back \n\
	0 - Custom \n\
	1 - Figure 8 around two planets \n\
	2 - Oval flower around two planets \n\
	3 - Central flower "))

		if versionSelect in validVersion:
			goodMenu = False

	if (versionSelect == "B") or (versionSelect == "b"):
		print (" ")
	elif int(versionSelect) == 0:
		a = getPlanetMass()
		b = getPlanetRadius()
		c = getOrbitHeight()
		d = getSatSpeed()
		e = getDirection()
		f = getBoost()
		g = getLengthOfBoost()
		h = getNewPlanetX()
		i = getNewPlanetY()
		orbit4.orbit(a, b, c, d, e, f, g, h, i)
	elif int(versionSelect) == 1:
		orbit4.orbit(20,15,180,4,86,0,0,300,300)
	elif int(versionSelect) == 2:
		orbit4.orbit(20,15,180,4,80,0,0,200,0)
	elif int(versionSelect) == 3:		
		orbit4.orbit(20,15,180,4,86,0,0, 0, 50)


validVersion = ["1", "2", "3", "4"]

while True:
	goodMenu = True
	while goodMenu == True:
		versionSelect = str(input("Select verion number: \n\
	1 - One planet, no angle \n\
	2 - One planet, angle of satelite \n\
	3 - One planet, angle of satelite and boost \n\
	4 - Two planets and satelite angle "))
		if versionSelect in validVersion:
			goodMenu = False

	if int(versionSelect) == 1:
		choose1()
	elif int(versionSelect) == 2:
		choose2()
	elif int(versionSelect) == 3:
		choose3()
	elif int(versionSelect) == 4:
		choose4()

