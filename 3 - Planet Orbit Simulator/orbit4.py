import turtle as t
import time
import math

def drawPlanet(PlanetRadius, OrbitHeight, X, Y):
    t.penup()
    t.goto(X,(Y - PlanetRadius))
    t.pendown()
    t.begin_fill()
    t.circle(PlanetRadius)
    t.end_fill()

    #Gets pen in correct position
    t.penup()
    t.goto(0,OrbitHeight)
    t.pendown()

def initialSpeed(SatelliteSpeed, Direction):
    directionMod = Direction % 360
    if (directionMod >= 0) and (directionMod < 90):  
        xSpeed = SatelliteSpeed * math.sin(math.radians(directionMod))
        ySpeed = SatelliteSpeed * math.cos(math.radians(directionMod))
    elif (directionMod >= 90) and (directionMod < 180):
        directionMod -= 90
        xSpeed = SatelliteSpeed * math.cos(math.radians(directionMod))
        ySpeed = (0 - SatelliteSpeed * math.sin(math.radians(directionMod)))
    elif (directionMod >= 180) and (directionMod < 270):
        directionMod -= 180
        xSpeed = (0 - SatelliteSpeed * math.cos(math.radians(directionMod)))
        ySpeed = (0 - SatelliteSpeed * math.sin(math.radians(directionMod)))
    else:
        directionMod -= 270
        xSpeed = (0 - SatelliteSpeed * math.cos(math.radians(directionMod)))
        ySpeed = SatelliteSpeed * math.sin(math.radians(directionMod))

    return (xSpeed, ySpeed)

def boosters(Boost, xSpeed, ySpeed):
    if (xSpeed > 0) and (ySpeed < 0):
        (xBoost, yBoost) = (Boost, (0 - Boost))
    elif (xSpeed < 0) and (ySpeed < 0):
        (xBoost, yBoost) = ((0 - Boost), (0 - Boost))
    elif (xSpeed < 0) and (ySpeed > 0):
        (xBoost, yBoost) = ((0 - Boost), Boost)
    else:
        (xBoost, yBoost) = (Boost, Boost)

    return (xBoost, yBoost)

def orbit(PlanetMass,PlanetRadius,OrbitHeight,SatelliteSpeed, Direction, Boost, LengthOfBoost, NewPlanX, NewPlanY):
    G=100

    drawPlanet(PlanetRadius, OrbitHeight, 0, 0)
    drawPlanet(PlanetRadius, OrbitHeight, NewPlanX, NewPlanY)

    xPos = 0
    yPos = OrbitHeight

    (xSpeed, ySpeed) = initialSpeed(SatelliteSpeed, Direction)  

    (xBoost, yBoost) = (0, 0)

    distanceFromCentre1 = OrbitHeight
    distanceFromCentre = OrbitHeight

    while (distanceFromCentre > PlanetRadius) and (distanceFromCentre1 > PlanetRadius):

        t.goto(xPos,yPos)

        # Planet 2
        distanceFromCentre1 = math.sqrt(((xPos - NewPlanX)**2)+((yPos - NewPlanY)**2))

        gravX1 = ((G*PlanetMass) / (distanceFromCentre1)**2)*(abs(math.sin(math.atan((xPos - NewPlanX)/(yPos - NewPlanY)))))
        gravY1 = ((G*PlanetMass) / (distanceFromCentre1)**2)*(abs(math.cos(math.atan((xPos - NewPlanX)/(yPos - NewPlanY)))))

        if (xPos - NewPlanX) > 0:
            gravX1 = 0 - gravX1

        if (yPos - NewPlanY) > 0:
            gravY1 = 0 - gravY1





        # Planet 1
        distanceFromCentre = math.sqrt(((xPos)**2)+((yPos)**2))

        gravX = ((G*PlanetMass) / (distanceFromCentre)**2)*(abs(math.sin(math.atan(xPos/yPos))))
        gravY = ((G*PlanetMass) / (distanceFromCentre)**2)*(abs(math.cos(math.atan(xPos/yPos))))

        if xPos > 0:
            gravX = 0 - gravX

        if yPos > 0:
            gravY = 0 - gravY    





        # If used to improve performance
        if (Boost != 0) and (LengthOfBoost != 0):
                (xBoost, yBoost) = boosters(Boost, xSpeed, ySpeed)
                LengthOfBoost -= 1

        xAcceleration = gravX + gravX1
        yAcceleration = gravY + gravY1

        xSpeed = xSpeed + xAcceleration + xBoost
        ySpeed = ySpeed + yAcceleration + yBoost

        xPos = xPos + xSpeed
        yPos = yPos + ySpeed

        print (xSpeed, "      ", ySpeed)

        time.sleep(0.005)

    t.exitonclick()



# orbit4.orbit(20,15,180,4,86,0,0, 300, 300)
# Make flight controls

