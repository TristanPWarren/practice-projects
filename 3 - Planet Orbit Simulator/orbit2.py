import turtle as t
import time
import math

def drawPlanet(PlanetRadius, OrbitHeight):
    t.penup()
    t.goto(0,(0 - PlanetRadius))
    t.pendown()
    t.begin_fill()
    t.circle(PlanetRadius)
    t.end_fill()

    #Gets pen in correct position
    t.penup()
    t.goto(0,OrbitHeight)
    t.pendown()

def initialSpeed(SatelliteSpeed, Direction):
    directionMod = Direction % 360
    if (directionMod >= 0) and (directionMod < 90):  
        xSpeed = SatelliteSpeed * math.sin(math.radians(directionMod))
        ySpeed = SatelliteSpeed * math.cos(math.radians(directionMod))
    elif (directionMod >= 90) and (directionMod < 180):
        directionMod -= 90
        xSpeed = SatelliteSpeed * math.cos(math.radians(directionMod))
        ySpeed = (0 - SatelliteSpeed * math.sin(math.radians(directionMod)))
    elif (directionMod >= 180) and (directionMod < 270):
        directionMod -= 180
        xSpeed = (0 - SatelliteSpeed * math.cos(math.radians(directionMod)))
        ySpeed = (0 - SatelliteSpeed * math.sin(math.radians(directionMod)))
    else:
        directionMod -= 270
        xSpeed = (0 - SatelliteSpeed * math.cos(math.radians(directionMod)))
        ySpeed = SatelliteSpeed * math.sin(math.radians(directionMod))

    return (xSpeed, ySpeed)


def orbit(PlanetMass,PlanetRadius,OrbitHeight,SatelliteSpeed, Direction):
    G=100

    drawPlanet(PlanetRadius, OrbitHeight)

    xPos = 0
    yPos = OrbitHeight

    (xSpeed, ySpeed) = initialSpeed(SatelliteSpeed, Direction)   

    distanceFromCentre = OrbitHeight

    while (distanceFromCentre > PlanetRadius):

        t.goto(xPos,yPos)

        distanceFromCentre = math.sqrt((xPos**2)+(yPos**2))

        gravX = ((G*PlanetMass) / (distanceFromCentre)**2)*(abs(math.sin(math.atan(xPos/yPos))))
        gravY = ((G*PlanetMass) / (distanceFromCentre)**2)*(abs(math.cos(math.atan(xPos/yPos))))

        if xPos > 0:
            gravX = 0 - gravX

        if yPos > 0:
            gravY = 0 - gravY
        
        xAcceleration = gravX 
        yAcceleration = gravY  

        xSpeed = xSpeed + xAcceleration
        ySpeed = ySpeed + yAcceleration

        xPos = xPos + xSpeed
        yPos = yPos + ySpeed

        print ("X Speed : ", xSpeed, "    ", "Y Speed : ", ySpeed)

        time.sleep(0.005)

    t.exitonclick()


# orbit2.orbit(5,15,45,4,30)  <-- Turn off planet crashing
# Add accelration