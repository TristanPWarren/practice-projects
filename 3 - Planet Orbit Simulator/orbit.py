import turtle as t
import time
import math

def drawPlanet(PlanetRadius, OrbitHeight):
    t.penup()
    t.goto(0,(0 - PlanetRadius))
    t.pendown()
    t.begin_fill()
    t.circle(PlanetRadius)
    t.end_fill()

    #Gets pen in correct position
    t.penup()
    t.goto(0,OrbitHeight)
    t.pendown()

def orbit(SatelliteSpeed,OrbitHeight,PlanetMass,PlanetRadius):
    G=100

    drawPlanet(PlanetRadius, OrbitHeight)

    xPos = 0
    yPos = OrbitHeight

    xStep = SatelliteSpeed
    yStep = -((G*PlanetMass) / ((OrbitHeight**2)))

    distanceFromCentre = OrbitHeight

    theta = 0

    while (distanceFromCentre > PlanetRadius):

        t.goto(xPos,yPos)


        distanceFromCentre = math.sqrt((xPos**2)+(yPos**2))

        gravX = ((G*PlanetMass) / (distanceFromCentre)**2)*(abs(math.sin(math.atan(xPos/yPos))))
        gravY = ((G*PlanetMass) / (distanceFromCentre)**2)*(abs(math.cos(math.atan(xPos/yPos))))

        if xPos > 0:
            gravX = 0 - gravX

        if yPos > 0:
            gravY = 0 - gravY

        #Needs tidying
        if (xStep < 0) and (yStep < 0):
            theta = math.atan((0 - xStep)/(0 - yStep))
            speedStepX = (0 - (math.sin(theta)*SatelliteSpeed))
            speedStepY = (0 - (math.cos(theta)*SatelliteSpeed))
        elif (yStep < 0):
            theta = math.atan((0 - yStep)/xStep)
            speedStepX = math.cos(theta)*SatelliteSpeed
            speedStepY = (0 - (math.sin(theta)*SatelliteSpeed))
        elif (xStep < 0):  
            theta = math.atan((0 - xStep)/yStep)
            speedStepX = (0 - (math.sin(theta)*SatelliteSpeed))
            speedStepY = math.cos(theta)*SatelliteSpeed
        else:
            theta = math.atan(yStep/xStep)
            speedStepX = math.cos(theta)*SatelliteSpeed
            speedStepY = math.sin(theta)*SatelliteSpeed       

        xStep = speedStepX + gravX
        yStep = speedStepY + gravY

        xPos = xPos + xStep
        yPos = yPos + yStep

        # distanceFromCentre = math.sqrt((xPos**2)+(yPos**2))

        print(xPos, yPos, "         ", distanceFromCentre)

        time.sleep(0.005)

    t.exitonclick()

    


# orbit.orbit(7,50,20.79567080435)
# orbit.orbit(7,200,93.3069655,15)

# Speed is best at 0.005
# Angle of launch
# Make sure size of planet is reasonable