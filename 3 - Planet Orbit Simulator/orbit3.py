import turtle as t
import time
import math

def drawPlanet(PlanetRadius, OrbitHeight):
    t.penup()
    t.goto(0,(0 - PlanetRadius))
    t.pendown()
    t.begin_fill()
    t.circle(PlanetRadius)
    t.end_fill()

    #Gets pen in correct position
    t.penup()
    t.goto(0,OrbitHeight)
    t.pendown()

def initialSpeed(SatelliteSpeed, Direction):
    directionMod = Direction % 360
    if (directionMod >= 0) and (directionMod < 90):  
        xSpeed = SatelliteSpeed * math.sin(math.radians(directionMod))
        ySpeed = SatelliteSpeed * math.cos(math.radians(directionMod))
    elif (directionMod >= 90) and (directionMod < 180):
        directionMod -= 90
        xSpeed = SatelliteSpeed * math.cos(math.radians(directionMod))
        ySpeed = (0 - SatelliteSpeed * math.sin(math.radians(directionMod)))
    elif (directionMod >= 180) and (directionMod < 270):
        directionMod -= 180
        xSpeed = (0 - SatelliteSpeed * math.cos(math.radians(directionMod)))
        ySpeed = (0 - SatelliteSpeed * math.sin(math.radians(directionMod)))
    else:
        directionMod -= 270
        xSpeed = (0 - SatelliteSpeed * math.cos(math.radians(directionMod)))
        ySpeed = SatelliteSpeed * math.sin(math.radians(directionMod))

    return (xSpeed, ySpeed)

def boosters(Boost, xSpeed, ySpeed):
    if (xSpeed > 0) and (ySpeed < 0):
        (xBoost, yBoost) = (Boost, (0 - Boost))
    elif (xSpeed < 0) and (ySpeed < 0):
        (xBoost, yBoost) = ((0 - Boost), (0 - Boost))
    elif (xSpeed < 0) and (ySpeed > 0):
        (xBoost, yBoost) = ((0 - Boost), Boost)
    else:
        (xBoost, yBoost) = (Boost, Boost)

    return (xBoost, yBoost)

def orbit(PlanetMass,PlanetRadius,OrbitHeight,SatelliteSpeed, Direction, Boost, LengthOfBoost):
    G=100

    drawPlanet(PlanetRadius, OrbitHeight)

    xPos = 0
    yPos = OrbitHeight

    (xSpeed, ySpeed) = initialSpeed(SatelliteSpeed, Direction)  

    (xBoost, yBoost) = (0, 0)

    distanceFromCentre = OrbitHeight

    while (distanceFromCentre > PlanetRadius):

        t.goto(xPos,yPos)

        distanceFromCentre = math.sqrt((xPos**2)+(yPos**2))

        gravX = ((G*PlanetMass) / (distanceFromCentre)**2)*(abs(math.sin(math.atan(xPos/yPos))))
        gravY = ((G*PlanetMass) / (distanceFromCentre)**2)*(abs(math.cos(math.atan(xPos/yPos))))

        if xPos > 0:
            gravX = 0 - gravX

        if yPos > 0:
            gravY = 0 - gravY

        # If used to improve performance
        if (Boost != 0) and (LengthOfBoost != 0):
                (xBoost, yBoost) = boosters(Boost, xSpeed, ySpeed)
                LengthOfBoost -= 1

        xAcceleration = gravX 
        yAcceleration = gravY  

        xSpeed = xSpeed + xAcceleration + xBoost
        ySpeed = ySpeed + yAcceleration + yBoost 

        xPos = xPos + xSpeed
        yPos = yPos + ySpeed

        print ("X Speed : ", xSpeed, "  ", "Y Speed : ", ySpeed, "    ", LengthOfBoost)

        time.sleep(0.005)

    t.exitonclick()

# orbit3.orbit(12.3,15,30,7,100,0.001)
# orbit3.orbit(250,15,400,8,160,0,25)
# orbit3.orbit(12.3,15,30,7,100,0.001, 2800)


# Add another planet